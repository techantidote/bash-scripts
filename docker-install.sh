#!/bin/bash
echo "Post Upgrade Script"

# Basic Mon Tools
sudo apt install htop tmux -y

#Docker Pre-req
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y

#Download and install docker using convinence script
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Add current user to docker group
#echo "Adding user to group"

sudo usermod -aG docker $USER

# Re-login as user - Not required
#echo "Attempting to re-login as user"
#su $USER

echo "Enable Docker on Boot"
sudo systemctl enable docker

echo "Docker version would be printed for verificiation"
sudo su -c 'docker version' - $USER

echo "Docker Compose Installation"
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
echo "Compose Download complete, adding permission"
sudo chmod +x /usr/local/bin/docker-compose

echo "Verifying docker compose. Check if you are able to see compose version"
sudo su -c 'docker-compose version' - $USER
echo "Docker Compose Completed"

#End of docker compose installation



