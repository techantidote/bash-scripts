# Bash aliases
alias l='ls -l --color=auto'
alias ll='ls -l'
alias r='ranger'
alias grep='grep --color=auto'
alias v='vim'

# Tmux stuff
alias ta='tmux attach -t'
alias tk='tmux kill-session -t'
alias tl='tmux list-sessions'
alias tn='tmux new -s'

# Other
alias xml-color='source-highlight -s xml -f esc  | less -R'
alias jz='jq -C . | less -R'
alias IP='curl ipinfo.io'

# Arch stuff
alias c='calcurse'
