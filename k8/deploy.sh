#!/bin/bash
source cluster_vars
source ec2_info

REGION="$(get_ec2_region)"
echo "Region: " $REGION

AZ="$(get_ec2_az)"
echo "EC2 deployed in AZ: " $AZ

ZONES="$(show_az_in_region "$REGION")"
echo "Zones in AZ: " $ZONES

# Variables that kops expects are:

## Cluster Name
#NAME

## S3 Bucket
#KOPS_STATE_STORE

## Zones - The AZ where the cluster can be deployed
#ZONES


