#!/bin/bash

# Enable AWS autocompletion
echo "complete -C '/home/ubuntu/.local/bin/aws_completer' aws" >> ~/.bashrc

# Setup SSH keys
ssh-keygen -t rsa -f ~/.ssh/id_rsa -P ""

# Setup AWS
aws configure

