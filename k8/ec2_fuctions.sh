create_private_r53_zone()
{ # Usage: create_r53_zone $R53_ZONE_NAME $file_name
	local R53_ZONE_NAME=$1
	local VPC_REGION=$2
	local VPC_ID=$3
	local file_name=$4
	local ID=$(uuidgen)
	aws route53 create-hosted-zone --name $R53_ZONE_NAME --caller-reference $ID --vpc VPCRegion="$VPC_REGION",VPCId="$VPC_ID" | jq .DelegationSet.NameServers > $file_name
	echo "R53 Zone creation output stored in:" $file_name
}


create_public_r53_zone()
{ # Usage: create_public_r53_zone $R53_ZONE_NAME $file_name
	local R53_ZONE_NAME=$1
	local file_name=$2
	local ID=$(uuidgen)
	aws route53 create-hosted-zone --name $R53_ZONE_NAME --caller-reference $ID --hosted-zone-config PrivateZone=false | jq .DelegationSet.NameServers > $file_name
	echo "R53 Zone creation output stored in:" $file_name
}


get_r53_zone_id() # Not working
{ # Usage: get_r53_zone_id $zone_name $R53_ZONE_NAME | Output: Zone ID
	local zone_name=$1
	local R53_ZONE_NAME=$zone_name.
	local zone_id="$(aws route53 list-hosted-zones | jq '.HostedZones[] | select(.Name=="$R53_ZONE_NAME") | .Id')"
	echo $zone_id
}


# Examples
#create_private_r53_zone "$R53_ZONE_NAME" "$VPC_REGION" "$VPC_ID" "$FILE_NAME"
#R53_PUBLIC_ZONE_NAME="k8testing123.com"
#FILE_NAME="r53.out"
#create_public_r53_zone "$R53_PUBLIC_ZONE_NAME" "$FILE_NAME"
#get_r53_zone_id "k8testing123.com"
