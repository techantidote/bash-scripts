get_ec2_region()
{

        local REGION="$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq . | grep region | awk '{print $2}' | tr -d '"')"
        echo $REGION
}

get_ec2_az()
{
        CURRENT_AZ="$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq . | grep region | awk '{print $2}' | tr -d '"')"
        echo $CURRENT_AZ
}

show_az_in_region()
{
        local REGION=$1
        local ZONES=$(aws ec2 describe-availability-zones --region $REGION | jq '.AvailabilityZones[].ZoneName' | tr -d '"')
        echo $ZONES
}
