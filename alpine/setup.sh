#!/bin/sh
# Tested on Alpine 3.8.4 || Kernel Version: 4.14.104-0-vanilla
## Install basic packages
apk add vim tmux curl git htop jq tree less -y
echo PermitRootLogin yes >> /etc/ssh/sshd_config
#Generate Keys
#ssh-keygen -f id_rsa -t rsa -N ''
ssh-keygen -f /root/.ssh/id_rsa -t rsa -N ''
echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories
apk update 
apk add docker -y
rc-update add docker boot
service docker start
#Install docker compose
apk add py-pip
pip install docker-compose

# Isolate containers with namespaces
adduser -SDHs /sbin/nologin dockremap
addgroup -S dockremap
echo dockremap:$(cat /etc/passwd|grep dockremap|cut -d: -f3):65536 >> /etc/subuid
echo dockremap:$(cat /etc/passwd|grep dockremap|cut -d: -f4):65536 >> /etc/subgid
cat >> /etc/docker/daemon.json <<EOF
{  
        "userns-remap": "dockremap"
}
EOF

# Enable Cgroups
echo "cgroup /sys/fs/cgroup cgroup defaults 0 0" >> /etc/fstab

cat >> /etc/cgconfig.conf <<EOF
mount {
cpuacct = /cgroup/cpuacct;
memory = /cgroup/memory;
devices = /cgroup/devices;
freezer = /cgroup/freezer;
net_cls = /cgroup/net_cls;
blkio = /cgroup/blkio;
cpuset = /cgroup/cpuset;
cpu = /cgroup/cpu;
}
EOF

# Enable cgroup condition
sed -i 's/default_kernel_opts=.*/default_kernel_opts="nomodeset pax_nouderef quiet rootfstype=ext4 cgroup_enable=memory swapaccount=1"/g' /etc/update-extlinux.conf
# Update config and then reboot
update-extlinux
