#!/bin/bash
# Script to download install Golang

# Update Golang DDL accordingly.
# This is not the src filename
GO_DDL_LINK="https://dl.google.com/go/go1.15.6.linux-amd64.tar.gz"

# Get filename from Download link and store in variable
GO_DDL_FILE_NAME="$(basename $GO_DDL_LINK)"

#echo $GO_DDL_FILE_NAME

wget $GO_DDL_LINK
sudo tar -C /usr/local -xzf $GO_DDL_FILE_NAME
echo "export PATH=$PATH:/usr/local/go/bin" | sudo tee -a /etc/profile
export PATH=$PATH:/usr/local/go/bin
go version
