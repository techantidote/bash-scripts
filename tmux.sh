#!/bin/bash
# Tmux install script

TMUX_DDL="wget https://github.com/tmux/tmux/releases/download/3.3a/tmux-3.3a.tar.gz"

sudo apt update -y
sudo apt install wget gcc libevent-2.1* libevent-dev libncurses5 libncurses5-dev make -y

cd ~/
wget $TMUX_DDL

tar zxvf ~/tmux-3.3a.tar.gz
cd ~/tmux-3.3a
./configure && make
