#!/bin/bash
# Script to install packages from a file

#my PKG_FILE=(basic.txt analysis.txt dev-tools.txt optional.txt)
PKG_FILE="fedora-packages.txt"

 while read -r line
do
echo "Installing Package-->$line"
sudo yum install -y  $line
done < $PKG_FILE