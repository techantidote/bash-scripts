#!/bin/bash
echo "Initial Setup Script"
# Add user to nopasswd
sudo usermod -aG sudo $USER
echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/90-custom-user

# Update and upgrade host sysem
sudo apt update -y && sudo apt upgrade -y

#System alternatives
sudo apt install vim -y
sudo update-alternatives --set editor /usr/bin/vim.basic

echo "Post Upgrade Script"

# Basic Tools
sudo apt install htop tmux wget jq -y

#Docker Pre-req
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y

#Download and install docker using convinence script
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Add current user to docker group
#echo "Adding user to group"

sudo usermod -aG docker $USER

echo "Enable Docker on Boot"
sudo systemctl enable docker

echo "Docker version would be printed for verificiation"
sudo su -c 'docker version' - $USER

echo "Docker Compose Installation"
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
echo "Compose Download complete, adding permission"
sudo chmod +x /usr/local/bin/docker-compose

echo "Verifying docker compose. Check if you are able to see compose version"
sudo su -c 'docker-compose version' - $USER
echo "Docker Compose Completed"

# Import the GPG repository key to protect yourself against downloading Kubernetes 
sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
# Add K8 repo
echo 'deb http://apt.kubernetes.io/ kubernetes-xenial main' | sudo tee /etc/apt/sources.list.d/kubernetes.list
# Add kubelet kubeadm
sudo apt update && sudo apt install -y kubelet kubeadm kubernetes-cni
echo "End of installation. Type exit and re-login via SSH"
