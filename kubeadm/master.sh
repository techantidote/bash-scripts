#!/bin/bash
RED='\033[0;31m'
NC='\033[0m' # No Color

echo "Kubernetes Master Setup"
sudo kubeadm init --node-name master
echo -e "I ${RED}Copy the above join command${NC}"
sudo mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
echo "check nodes + Pods"
kubectl get nodes -o wide
kubectl get pods --all-namespaces
hostname="$(curl http://169.254.169.254/latest/meta-data/local-hostname)"
echo "Setup overlay network - Weave"
export kubever=$(sudo kubectl version | base64 | tr -d '\n')
sudo kubectl apply -f “https://cloud.weave.works/k8s/net?k8s-version=$kubever"
echo "alias k='sudo kubectl'" >> ~/.bash_aliases
source ~/.bashrc
echo -e "I ${RED}Now, login to the worker node and use the join token${NC}"
