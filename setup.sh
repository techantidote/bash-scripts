#!/bin/bash

echo "Initial Setup Script"

# Add user to sudo group
sudo usermod -aG sudo $USER
echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/90-custom-user

# Update and upgrade host sysem
sudo apt update -y && sudo apt upgrade -y

# System alternatives
sudo apt install vim -y
sudo update-alternatives --set editor /usr/bin/vim.basic

echo "Post Upgrade Script"

# Basic Tools
sudo apt install htop tmux wget jq ranger curl tree openssh-server -y

# Compilers / Rev Tools
sudo apt install gcc make gdb -y

# Docker Pre-req
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y

# Download and install docker using convinence script
echo "Installing Docker... May take a bit...Hang on m8.."
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Add current user to docker group
#echo "Adding user to group"

sudo usermod -aG docker $USER

# Re-login as user - Not required
#echo "Attempting to re-login as user"
#su $USER

echo "Enable Docker on Boot"
sudo systemctl enable docker

echo "Docker version would be printed for verificiation"
sudo su -c 'docker version' - $USER

# New docker install has Docker compose installed so its not needed
#echo "Docker Compose Installation"
#sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/#docker-compose
#echo "Compose Download complete, adding permission"
#sudo chmod +x /usr/local/bin/docker-compose
#echo "Verifying docker compose. Check if you are able to see compose version"
#sudo su -c 'docker-compose version' - $USER
#echo "Docker Compose Completed"

sudo su -c 'docker compose version' - $USER


#./copy-dot-files.sh

#End of docker compose installation
exit

