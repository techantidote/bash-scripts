#!/bin/bash
echo "Initial Setup Script"

# Add user to nopasswd
sudo usermod -aG sudo $USER
echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/90-custom-user

# Update and upgrade host sysem
sudo apt update -y && sudo apt upgrade -y

#System alternatives
sudo apt install vim -y
sudo update-alternatives --set editor /usr/bin/vim.basic

# Basic Mon Tools
sudo apt install htop tmux -y

# Add Aliases
echo "alias ta='tmux attach -t'" >> ~/.bashrc
echo "alias tk='tmux kill-session -t'" >> ~/.bashrc
echo "alias tl='tmux list-sessions'" >> ~/.bashrc
echo "alias tn='tmux new -s'" >> ~/.bashrc
echo "alias jz='jq -C . | less -R'" >> ~/.bashrc

# Install vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Copy vim confs
cp dotfiles/vimrc ~/.vimrc
# Install Vim Plugins
vim +PluginInstall +qall

### Tmux 
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
cp dotfiles/tmux.conf ~/.tmux.conf

# Install tmux plugins

~/.tmux/plugins/tpm/bin/install_plugins

#Reboot
sudo reboot now
