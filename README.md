# Bash Scripts

This is a collection of Bash scripts that can be used to automate intial setups and software installs.

## Ubuntu/Linux Mint Scripts:

- The script "setup.sh" is used to setup local dev environment.

```
git clone https://gitlab.com/techantidote/bash-scripts/
cd bash-scripts/
chmod +x setup.sh
./setup.sh
```

- Go Lang install: 

```
cd bash-scripts/
./go-setup.sh
```


### Contact Information

-Would you like to contribute to this project? Or if you have any feedback, you can reach out to me in twitter.

https://twitter.com/techantidote
