#!/bin/bash
# This fix for ubuntu 18.04

echo "DNSStubListener=no" | sudo tee -a /etc/systemd/resolved.conf

netstat -ntpl | grep :53

sudo service systemd-resolved stop
sudo rm -f /etc/resolv.conf
sudo ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
sudo service systemd-resolved start

sudo reboot now

# Sources/Credit:
#https://aacable.wordpress.com/2019/12/10/short-notes-for-unbound-caching-dns-server-under-ubuntu-18/
