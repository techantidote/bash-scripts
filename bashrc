# Aliases with file handling
# Add these at end of ~/.bashrc
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.wsl_browser_aliases ]; then
    . ~/.wsl_browser_aliases
fi

if [ -f ~/.work_aliases ]; then
    . ~/.work_aliases
fi

if [ -f ~/.watchtower_aliases ]; then
    . ~/.watchtower_aliases
fi
