#!/bin/bash

echo "Initial Setup Script"

# Add user to nopasswd
sudo usermod -aG sudo $USER
echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/90-custom-user

# Update and upgrade host sysem
sudo apt update -y && sudo apt upgrade -y

#System alternatives
sudo apt install vim -y
sudo update-alternatives --set editor /usr/bin/vim.basic

# Basic Mon Tools
sudo apt install htop tmux -y
#Rebot
#sudo reboot now
echo "Post Upgrade Script"

sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y
sudo apt install jq python-pip -y

# Install Kops
wget https://github.com/kubernetes/kops/releases/download/1.10.0/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/local/bin/kops


# Install kubectl client tool
curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

# Install aws cli
pip install awscli

# Setup SSH keys
ssh-keygen -t rsa -f ~/.ssh/id_rsa -P ""
